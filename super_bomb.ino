#include <Keypad.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h> ;

/*
  Arduino Fake Bomb

*/

char* menu1[] = { "TROVA E DISARMA",
                  " SABOTAGGIO",
                  " DOMINAZIONE",
                  " IMPOSTARE" };
char* menu2[] = { "IMPOSTA GIOCO",
                  "ATTIVA SUONO",
                  "TEST DEL RELE",
                  "AUTO TEST " };
char* GAME_TIME = "TEMPO DI GIOCO:";
char* TIME_OVER = "TEMPO ESAURITO";
char* SABOTAGE_OK = "SABOTAGE OK!";
char* PLAY_AGAIN = "GIOCA DI NUOVO?";
char* BOMB_TIME = "TEMPO BOMBA:";
char* COUNTDOWN_TIME = "TEMPO COUNTDOWN:";
char* ZERO_MINUTES = "00 MINUTI";
char* ARM_TIME = "TEMPO PER ARMARE:";
char* ZERO_SECS = "00 SECONDI";
char* ENABLE_SOUND = "ABILITA SUONO?";
char* YES_OR_NOT = "A : SI B : NO";
char* ENABLE_RELAYPIN = "ABILITA RELE?";
char* ENABLE_CODE = "ABILITA PW?";
char* ENABLE_WIRE = "ABILITA CAVI?";
char* ENABLE_GRENADE = "ABILITA GRANATA?";
char* GAME_TIME_TOP = "TEMPO DI GIOCO";
char* ARMING_BOMB = "ARMAMENTO BOMBA";
char* ENTER_CODE = "INSERISCI PW";
char* CODE_ERROR = "ERRORE PW!";
char* WIRE_CODE_ERROR = "CAVO ERRATO!";
char* BOMB_ARMED = "BOMBA ARMATA";
char* DETONATION_IN = "DETONAZIONE..";
char* COUNTDOWN_IN = "COUNTDOWN..";
char* DISARMA = "DISARMO BOMBA";
char* DISARM = "BOMBA DISARMATA";
char* GAME_OVER = "GAME OVER!";
char* BOOM = "BOOM!!!";
char* DEFENDERS_WIN = " WINNER! ";
char* SABOTAGE_FAIL = "SABOTAGE FAIL!";
char* WIRE_CHOOSE = "SCELGLI COLORE";
char* WIRE_SELECTION = "A:Y B:G C:B D:V";

LiquidCrystal_I2C lcd(0x27, 16, 2);
// LiquidCrystal_I2C lcd(0x27,20,4);
const byte ROWS = 4; // four rows
const byte COLS = 4; // three columns
char keys[ROWS][COLS] = { { '1', '2', '3', 'A' },
                          { '4', '5', '6', 'B' },
                          { '7', '8', '9', 'C' },
                          { '*', '0', '#', 'D' } };

byte rowPins[ROWS] = { 12,
                       13,
                       A5,
                       A4 }; // connect to the row pinouts of the keypad
byte colPins[COLS] = { A3,
                       A2,
                       A1,
                       A0 }; // connect to the column pinouts of the keypad

Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

char codeInput[8];
byte time[4];
boolean refresh = true; // 1 refresh one time...
char password[8];
int key = -1;
char lastKey;
char var;
boolean passwordEnable = false;
boolean wireEnable = false;
boolean grenadeEnable = false;
int correctWire = -1;

// Buttons for lcd shield
char BT_RIGHT = '4';
char BT_UP = 'A';
char BT_DOWN = 'B';
char BT_LEFT = '6';
char BT_SEL = 'D'; // Ok key
char BT_CANCEL = 'C';
char BT_DEFUSER = 'X'; // not implemented

// LEDS
const int REDLED = 11;
const int GREENLED = 10;
// LED STRIPE
const int STRIPELED = 2;
// GRENADE
const int GRENADE = 3;

// RELAYPIN
boolean relayEnable = false;
// IS VERY IMPORTANT THAT YOU TEST THIS TIME. BY DEFAULT IS IN 1 SEC. THAT IS
// NOT TOO MUCH. SO TEST IT!
const int RELAY_TIME = 5000;
// Wires
const int VIOLET_WIRE = 7;
const int BLUE_WIRE = 6;
const int GREEN_WIRE = 5;
const int YELLOW_WIRE = 4;

// TIME INTS
int GAMEHOURS = 0;
int GAMEMINUTES = 45;
int BOMBMINUTES = 4;
int ACTIVATESECONDS = 5;
int COUNTDOWNMINUTES = 5;

boolean endGame = false;
boolean defused = false;

boolean sdStatus = false; // search and destroy game enable used in config
boolean saStatus = false; // same but SAbotaghe
boolean doStatus = false; // for DEmolition
boolean start = true;
boolean defusing;
boolean cancelando;
boolean errorBeforeCode = false;
boolean blueWireOpened = false;
boolean greenWireOpened = false;
boolean yellowWireOpened = false;
boolean violetWireOpened = false;
int wireErrors = 0;
// SOUND TONES
boolean soundEnable = true;
int tonepin = 9;
int alarmTone1 = 700;
int alarmTone2 = 2600;
int activeTone = 1330;
int errorTone = 100;

char stripeState = LOW;
unsigned long nextStripeBlink;
unsigned long iTime;
unsigned long timeCalcVar;
unsigned long redTime;
unsigned long greenTime;
unsigned long iZoneTime; // initial time for zone
byte team =
  0; // 0 = neutral, 1 = green team, 2 = red
     // team...........................................................................................................

void
setup()
{
  Serial.begin(9600); // set up Serial library at 9600 bps
  Serial.println(" IL DON");

  lcd.init();
  lcd.backlight();

  lcd.begin(16, 2);
  lcd.print("ELITE SOLDIERS");
  lcd.setCursor(0, 1);
  lcd.print("  SOFTAIR TEAM");

  delay(500);
  lcd.begin(16, 2);
  lcd.print("  AIRSOFT FAKE");
  lcd.setCursor(0, 1);
  lcd.print("**BOMB**byDon");
  delay(500);
  lcd.begin(16, 2);
  lcd.setCursor(3, 0);
  tone(tonepin, 2400, 30);
  lcd.print("BLUECORE TECH"); // you can add your team name or someting cool
  lcd.setCursor(0, 1);
  lcd.print(" AIRSOFT BOMB"); // you can add your team name or someting cool
  keypad.setHoldTime(50);
  keypad.setDebounceTime(50);
  keypad.addEventListener(keypadEvent);

  // PinModes
  pinMode(GREENLED, OUTPUT);
  pinMode(REDLED, OUTPUT);
  pinMode(STRIPELED, OUTPUT);
  pinMode(GRENADE, OUTPUT);
  pinMode(YELLOW_WIRE, INPUT);
  pinMode(GREEN_WIRE, INPUT);
  pinMode(BLUE_WIRE, INPUT);
  pinMode(VIOLET_WIRE, INPUT);
}

void
loop()
{
  menuPrincipal();
}

void
keypadEvent(KeypadEvent key)
{
  switch (keypad.getState()) {
    case RELEASED:
      switch (key) {
        case 'D':
          defusing = false;
          break;
        case 'C':
          cancelando = false;
          break;
      }
      break;
    case HOLD:
      switch (key) {
        case 'D':
          defusing = true;
          break;
        case 'C':
          cancelando = true;
          break;
      }
      break;
  }
}
//##################MENUS###############################

void
menuPrincipal()
{ // MAIN MENU

  digitalWrite(GREENLED, LOW);
  digitalWrite(REDLED, LOW);
  digitalWrite(STRIPELED, LOW);

  analogWrite(GRENADE, 0);

  //   if whe start a new game from another we need to restart propertly this
  //   variables
  saStatus = false;
  sdStatus = false;
  doStatus = false;
  // Draw menu
  cls(); // clear lcd and set cursor to 0,0
  int i = 0;
  // HERE YOU CAN ADD MORE ITEMS ON THE MAIN MENU
  lcd.print(menu1[i]);
  lcd.setCursor(15, 1);
  checkArrows(i, 2);
  while (1) {

    var = keypad.waitForKey();
    Serial.println(var);
    if (var == BT_UP && i > 0) {
      tone(tonepin, 2400, 30);
      i--;
      cls();
      lcd.print(menu1[i]);
      checkArrows(i, 2);
      delay(50);
    }
    if (var == BT_DOWN && i < 2) {
      tone(tonepin, 2400, 30);
      i++;
      cls();
      lcd.print(menu1[i]);
      checkArrows(i, 2);
      delay(50);
    }

    if (var == BT_SEL) {
      tone(tonepin, 2400, 30);
      cls();
      switch (i) {

        case 0:
          sdStatus = true;
          configQuickGame();
          startGameCount();
          search();
          break;
        case 1:
          saStatus = true;
          configQuickGame();
          startGameCount();
          sabotage();
          break;
        case 2:

          doStatus = true;
          configQuickGame();
          startGameCount();
          domination();
          break;
        case 3:
          config();
          break;
      }
    }
  }
}

void
config()
{
  // Draw menu

  lcd.clear();
  lcd.setCursor(0, 0);
  int i = 0;

  delay(500);
  lcd.print(menu2[i]);
  checkArrows(i, 3);

  while (1) {
    var = keypad.waitForKey();
    if (var == BT_UP && i > 0) {
      tone(tonepin, 2400, 30);
      i--;
      lcd.clear();
      lcd.print(menu2[i]);
      checkArrows(i, 3);
      delay(50);
    }
    if (var == BT_DOWN && i < 3) {
      tone(tonepin, 2400, 30);
      i++;
      lcd.clear();
      lcd.print(menu2[i]);
      checkArrows(i, 3);
      delay(50);
    }
    if (var == BT_CANCEL) {
      tone(tonepin, 2400, 30);
      menuPrincipal();
    }
    if (var == BT_SEL) {
      tone(tonepin, 2400, 30);
      lcd.clear();
      switch (i) {

        case 0:
          // gameConfigMenu();
          break;

        case 1:
          // soundConfigMenu();
          break;

        case 2:
          cls();
          lcd.print("RELAYPIN ON!");
          delay(4000); // wait for 4 second
          cls();
          lcd.print("RELAYPIN OFF!");
          delay(2000);
          config();
          break;
      }
    }
  }
}

void
configQuickGame()
{

  cls();
  // GAME TIME
  if (sdStatus || doStatus || saStatus) {
  menu1:
    cls();
    lcd.print(GAME_TIME);
    delay(100);
    lcd.setCursor(0, 1);
    lcd.print("00:00 hh:mm");
    lcd.cursor();
    lcd.blink();
    lcd.setCursor(0, 1);
    byte var2 = 0;
    for (int i = 0; i < 4; i++) {
      while (1) {
        if (i == 2 && var2 == 0) {
          lcd.print(":");
          var2 = 1;
        }

        byte varu = getRealNumber();
        if (varu != 11) {

          time[i] = varu;
          Serial.print(varu);

          lcd.print(varu);
          tone(tonepin, 2400, 30);

          break;
        }
      }
    }
    lcd.noCursor();
    lcd.noBlink();
    lcd.setCursor(13, 1);
    lcd.print("ok?");
    // zona donde pasamos los items a
    // redibujar
    while (1) {
      var = keypad.waitForKey();
      if (var == 'D') // Accept
      {
        tone(tonepin, 2400, 30);
        GAMEMINUTES =
          ((time[0] * 600) + (time[1] * 60) + (time[2] * 10) + (time[3]));
        break;
      }
      if (var == 'C') // Cancel or Back Button :')
      {
        tone(tonepin, 2400, 30);
        goto menu1;
      }
    }
    tone(tonepin, 2400, 30);
    cls();
  }
  // BOMB TIME
  if (sdStatus || saStatus) {

  menu2:
    cls();
    lcd.print(BOMB_TIME);
    delay(100);
    lcd.setCursor(0, 1);
    lcd.print(ZERO_MINUTES);
    lcd.cursor();
    lcd.blink();
    lcd.setCursor(0, 1);
    for (int i = 0; i < 2; i++) {
      while (1) {
        byte varu = getRealNumber();
        if (varu != 11) {
          time[i] = varu;
          lcd.print(varu);
          tone(tonepin, 2400, 30);
          break;
        }
      }
    }
    lcd.noCursor();
    lcd.noBlink();
    lcd.setCursor(13, 1);
    lcd.print("ok?");
    // zona donde pasamos los items a
    // redibujar
    while (1) {
      var = keypad.waitForKey();
      if (var == 'D') //
      {
        tone(tonepin, 2400, 30);
        BOMBMINUTES = ((time[0] * 10) + (time[1]));
        break;
      }
      if (var == 'C') // Cancel or Back Button :')
      {
        tone(tonepin, 2400, 30);
        goto menu2;
      }
    }
    tone(tonepin, 2400, 30);
    cls();
  }
  cls();
  // ARMING TIME
  if (sdStatus || doStatus || saStatus) {

  menu3:
    cls();
    lcd.print(ARM_TIME);
    delay(100);
    lcd.setCursor(0, 1);
    lcd.print(ZERO_SECS);
    lcd.cursor();
    lcd.blink();
    lcd.setCursor(0, 1);
    for (int i = 0; i < 2; i++) {
      while (1) {
        byte varu = getRealNumber();
        if (varu != 11) {
          time[i] = varu;
          lcd.print(varu);
          tone(tonepin, 2400, 30);
          break;
        }
      }
    }
    lcd.noCursor();
    lcd.noBlink();
    lcd.setCursor(13, 1);
    lcd.print("ok?");

    // zona donde pasamos los items a
    // redibujar
    while (1) {
      var = keypad.waitForKey();
      if (var == 'D') // Accept
      {
        tone(tonepin, 2400, 30);
        ACTIVATESECONDS = ((time[0] * 10) + (time[1]));
        break;
      }
      if (var == 'C') // Cancel or Back Button :')
      {
        tone(tonepin, 2400, 30);
        goto menu3;
      }
    }
    tone(tonepin, 2400, 30);
    cls();
  }

  // COUNTDOWN TIME
  if (saStatus) {
  menu4:
    cls();
    lcd.print(COUNTDOWN_TIME);
    delay(100);
    lcd.setCursor(0, 1);
    lcd.print(ZERO_MINUTES);
    lcd.cursor();
    lcd.blink();
    lcd.setCursor(0, 1);
    for (int i = 0; i < 2; i++) {
      while (1) {
        byte varu = getRealNumber();
        if (varu != 11) {
          time[i] = varu;
          lcd.print(varu);
          tone(tonepin, 2400, 30);
          break;
        }
      }
    }
    lcd.noCursor();
    lcd.noBlink();
    lcd.setCursor(13, 1);
    lcd.print("ok?");
    // zona donde pasamos los items a
    // redibujar
    while (1) {
      var = keypad.waitForKey();
      if (var == 'D') //
      {
        tone(tonepin, 2400, 30);
        COUNTDOWNMINUTES = ((time[0] * 10) + (time[1]));
        break;
      }
      if (var == 'C') // Cancel or Back Button :')
      {
        tone(tonepin, 2400, 30);
        goto menu4;
      }
    }
    tone(tonepin, 2400, 30);
    cls();
  }
  cls();
  // sound??
  if (sdStatus || saStatus || doStatus) {
    cls();
    lcd.print(ENABLE_SOUND);
    lcd.setCursor(0, 1);
    lcd.print(YES_OR_NOT);

    while (1) {
      var = keypad.waitForKey();
      if (var == 'A') {
        soundEnable = true;
        tone(tonepin, 2400, 30);
        break;
      }

      if (var == 'B') {
        soundEnable = false;
        tone(tonepin, 2400, 30);
        break;
      }
    }
  }
  // Activate RELAY at Terrorist game ends??? Boom!

  if (sdStatus || saStatus) {
    cls();
    lcd.print(ENABLE_RELAYPIN);
    lcd.setCursor(0, 1);
    lcd.print(YES_OR_NOT);
    while (1) {
      var = keypad.waitForKey();
      if (var == 'A') {
        relayEnable = true;
        tone(tonepin, 2400, 30);
        break;
      }
      if (var == 'B') {
        relayEnable = false;
        tone(tonepin, 2400, 30);
        break;
      }
    }
  }
  // You Want a password enable-disable game?
  if (sdStatus || saStatus) {
    cls();
    lcd.print(ENABLE_CODE);
    lcd.setCursor(0, 1);
    lcd.print(YES_OR_NOT);

    while (1) {
      var = keypad.waitForKey();
      if (var == 'A') {
        tone(tonepin, 2400, 30);
        setNewPass();
        passwordEnable = true;
        break;
      }
      if (var == 'B') {
        tone(tonepin, 2400, 30);
        passwordEnable = false;
        break;
      }
    }
    tone(tonepin, 2400, 30);
  }
  // You Want a cable disarming game?
  if (sdStatus || saStatus) {
    cls();
    lcd.print(ENABLE_WIRE);
    lcd.setCursor(0, 1);
    lcd.print(YES_OR_NOT);

    while (1) {
      var = keypad.waitForKey();
      if (var == 'A') {
        tone(tonepin, 2400, 30);
        enableWire();
        wireEnable = true;
        break;
      }
      if (var == 'B') {
        tone(tonepin, 2400, 30);
        wireEnable = false;
        break;
      }
    }
    tone(tonepin, 2400, 30);
  }

  // You Want to explode the grenade if loosing?
  if (sdStatus || saStatus) {
    cls();
    lcd.print(ENABLE_GRENADE);
    lcd.setCursor(0, 1);
    lcd.print(YES_OR_NOT);

    while (1) {
      var = keypad.waitForKey();
      if (var == 'A') {
        tone(tonepin, 2400, 30);
        grenadeEnable = true;
        break;
      }
      if (var == 'B') {
        tone(tonepin, 2400, 30);
        grenadeEnable = false;
        break;
      }
    }
    tone(tonepin, 2400, 30);
  }
  // Continue the game :D
}

void
domination()
{
  // SETUP INITIAL TIME
  int minutos = GAMEMINUTES - 1;
  boolean showGameTime = true;
  unsigned long a;
  unsigned long iTime = millis(); //  initialTime in millisec
  unsigned long aTime;

  team = 0;
  iZoneTime = 0;
  aTime = 0;
  redTime = 0;
  greenTime = 0;

  int largoTono = 50;
  // 0 = neutral, 1 = green team, 2 = red team
  a = millis();
  // Starting Game Code
  while (1) // this is the important code, is a little messy but works good.
  {
    if (endGame) {
      gameOver();
    }

    keypad.getKey();
    aTime = millis() - iTime;
    // Code for led blinking
    timeCalcVar = (millis() - iTime) % 1000;
    if (timeCalcVar >= 0 && timeCalcVar <= 40) {
      if (team == 1)
        digitalWrite(GREENLED, HIGH);
      if (team == 2)
        digitalWrite(REDLED, HIGH);
    }
    if (timeCalcVar >= 50 && timeCalcVar <= 100) {
      if (team == 1)
        digitalWrite(GREENLED, LOW);
      if (team == 2)
        digitalWrite(REDLED, LOW);
    }
    // Sound!!! same as Destroy
    if (timeCalcVar >= 0 && timeCalcVar <= 40 && soundEnable)
      tone(tonepin, activeTone, largoTono);

    if (timeCalcVar >= 245 && timeCalcVar <= 255 &&
        minutos - aTime / 60000 < 2 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (timeCalcVar >= 495 && timeCalcVar <= 510 &&
        minutos - aTime / 60000 < 4 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (timeCalcVar >= 745 && timeCalcVar <= 760 &&
        minutos - aTime / 60000 < 2 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    // Help to count 3 secs
    if (a + 2000 < millis()) {
      a = millis();
      showGameTime = !showGameTime;
      cls();
    }
    // THE NEXT TO METHODS SHOW "GAME TIME" AND "CONTROLED ZONE TIME" IT SHOWS 2
    // AND 2 SEC EACH

    if (showGameTime) { // THE SECOND IS /2
      lcd.setCursor(3, 0);
      lcd.print("TEMPO DI GIOCO");
      lcd.setCursor(3, 1);
      printTime(minutos, aTime);
    } else if (!showGameTime) {

      lcd.setCursor(2, 0);
      if (team == 0)
        lcd.print("ZONA NEUTRA");
      if (team == 1)
        lcd.print("ZONA VERDE");
      if (team == 2)
        lcd.print("ZONA ROSSA");

      if (team > 0) {
        lcd.setCursor(3, 1);
        printTimeDom(millis() - iZoneTime, true);
      }
    }

    //###########################CHECKINGS##################

    // Check If Game End
    if (minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) {
      gameOver();
    }

    // Check If IS neutral
    while ((defusing || cancelando) && team > 0) {
      cls();
      if (team > 0)
        lcd.print("NEUTRALIZANDO...");
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis(); // start disabling time
      while (defusing || cancelando) {
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000) {
          endGame = true;
        }

        keypad.getKey();
        timeCalcVar = (millis() - xTime) % 1000;

        if (timeCalcVar >= 0 && timeCalcVar <= 20) {
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 500) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(REDLED, LOW);
        }

        unsigned long seconds = millis() - xTime;
        percent = (seconds) / (ACTIVATESECONDS * 10);
        drawBar(percent);

        if (percent >= 100) {
          delay(1000);

          if (team == 1) {
            greenTime += millis() - iZoneTime;
            iZoneTime = 0;
          }
          if (team == 2) {
            redTime += millis() - iZoneTime;
            iZoneTime = 0;
          }
          team = 0;
          break;
        }
      }
      cls();
    }

    // Capturing red

    while (defusing && team == 0) {
      cls();
      if (team == 0)
        lcd.print("ZONA CONQUISTATA");
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis(); // start disabling time
      while (defusing) {
        keypad.getKey();
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000) {
          endGame = true;
        }
        timeCalcVar = (millis() - xTime) % 1000;

        if (timeCalcVar >= 0 && timeCalcVar <= 20) {
          digitalWrite(REDLED, HIGH);
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 500) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(REDLED, LOW);
        }

        unsigned long seconds = millis() - xTime;
        percent = (seconds) / (ACTIVATESECONDS * 10);
        drawBar(percent);

        if (percent >= 100) {
          digitalWrite(GREENLED, LOW);
          team = 2;
          iZoneTime = millis();
          delay(1000);
          break;
        }
      }
      cls();
      digitalWrite(REDLED, LOW);
    }

    // getting to green zone
    while (cancelando && team == 0) {
      cls();
      if (team == 0)
        lcd.print("ZONA CONQUISTATA");
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis(); // start disabling time
      while (cancelando) {
        keypad.getKey();
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000) {
          endGame = true;
        }
        timeCalcVar = (millis() - xTime) % 1000;

        if (timeCalcVar >= 0 && timeCalcVar <= 20) {
          digitalWrite(GREENLED, HIGH);
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 500) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(GREENLED, LOW);
        }

        unsigned long seconds = millis() - xTime;
        percent = (seconds) / (ACTIVATESECONDS * 10);
        drawBar(percent);

        if (percent >= 100) {
          digitalWrite(GREENLED, LOW);
          team = 1;
          iZoneTime = millis();
          delay(1000);
          break;
        }
      }
      cls();
      digitalWrite(GREENLED, LOW);
    }
  }
}

void
gameOver()
{

  if (team == 1)
    greenTime += millis() - iZoneTime;
  if (team == 2)
    redTime += millis() - iZoneTime;
  digitalWrite(GREENLED, LOW);
  digitalWrite(REDLED, LOW);
  digitalWrite(STRIPELED, LOW);
  while (!defusing) {
    keypad.getKey();
    if (defusing) {
      keypad.getKey();
      break;
    }
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("TEMPO SCADUTO");
    lcd.setCursor(0, 1);

    // check who team win the base
    if (greenTime > redTime) {
      // greenteam wins
      lcd.print(" VITTORIA VERDE");
      digitalWrite(GREENLED, HIGH);
    } else {
      // redteam wins
      lcd.print("VITTORIA ROSSA");
      digitalWrite(REDLED, HIGH);
    }
    delay(3000);
    keypad.getKey();
    if (defusing) {
      keypad.getKey();
      break;
    }
    cls();
    lcd.print("TEMPO ROSSO:");
    lcd.setCursor(5, 1);
    printTimeDom(redTime, false);
    delay(3000);
    keypad.getKey();
    if (defusing) {

      break;
    }
    cls();
    lcd.print("TEMPO VERDE:");
    lcd.setCursor(5, 1);
    printTimeDom(greenTime, false);
    delay(3000);
    keypad.getKey();
    if (defusing) {
      keypad.getKey();
      break;
    }
  }
  endGameOrPlayAgain();
}
// This fuction compare codeInput[8] and password[8] variables
boolean
comparePassword()
{

  for (int i = 0; i < 8; i++) {
    if (codeInput[i] != password[i])
      return false;
  }
  return true;
}

void
sabotage()
{

  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  refresh = true;
  cls();
  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  // SETUP INITIAL TIME
  int minutos = GAMEMINUTES - 1;

  if (start) {
    iTime = millis(); //  initialTime of the game, use this because sabotage
                      //  mode goes can return to sabotage()
    start = false;
  }

  unsigned long aTime;

  // Starting Game Code
  while (1) { // this is the important code, is a little messy but works good.

    if (endGame) {
      failSplash();
    }
    // Code for led blinking
    timeCalcVar = (millis() - iTime) % 1000;
    if (timeCalcVar >= 0 && timeCalcVar <= 50)
      digitalWrite(GREENLED, HIGH);
    if (timeCalcVar >= 90 && timeCalcVar <= 130)
      digitalWrite(GREENLED, LOW);

    lcd.setCursor(3, 0);
    lcd.print(GAME_TIME);
    aTime = millis() - iTime;
    lcd.setCursor(3, 1);

    // PRINT TIME ON LCD
    printTime(minutos, aTime);
    //###########################CHECKINGS##################

    // Check If Game End
    if (minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) {
      failSplash();
    }

    // USED IN PASSWORD GAME
    if ('D' == keypad.getKey() && passwordEnable) {
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(ARMING_BOMB);
      delay(1000); // a little delay to think in the password
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(ENTER_CODE);

      setCodeTime(); // we need to set the compare variable first

      // then compare :D

      if (comparePassword()) {
        destroySabotage();
      }
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(CODE_ERROR);
      if (soundEnable)
        tone(tonepin, errorTone, 200);
      delay(500);
      cls();
    }

    // Check If Is Activating
    while (defusing && !passwordEnable) {
      keypad.getKey();
      cls();
      digitalWrite(GREENLED, LOW);
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(ARMING_BOMB);
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis(); // start disabling time
      while (defusing) {
        keypad.getKey();
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000)
          endGame = true;

        timeCalcVar = (millis() - xTime) % 1000;

        if (timeCalcVar >= 0 && timeCalcVar <= 40) {
          digitalWrite(REDLED, HIGH);
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 520) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(REDLED, LOW);
        }
        unsigned long seconds = millis() - xTime;
        percent = (seconds) / (ACTIVATESECONDS * 10);
        drawBar(percent);

        if (percent >= 100) {
          digitalWrite(GREENLED, LOW);
          destroySabotage(); // jump to the next gamemode
        }
      }
      cls();
      digitalWrite(REDLED, LOW);
    }
  }
}

void
destroySabotage()
{

  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print(BOMB_ARMED);
  delay(1000);
  int minutos = 0;

  if (defused) {
    minutos = COUNTDOWNMINUTES - 1;
  } else {
    minutos = BOMBMINUTES - 1;
  }

  unsigned long iTime = millis();
  unsigned long aTime;
  int largoTono = 50;

  // MAIN LOOP
  while (1) {
    stripeBlink();

    // If you fail disarm.
    if (endGame) {
      explodeSplash();
    }

    // Led Blink

    timeCalcVar = (millis() - iTime) % 1000;
    if (timeCalcVar >= 0 && timeCalcVar <= 40) {
      digitalWrite(REDLED, HIGH);
      if (soundEnable)
        tone(tonepin, activeTone, largoTono);
    }
    if (timeCalcVar >= 180 && timeCalcVar <= 220) {
      digitalWrite(REDLED, LOW);
    }
    // Sound

    timeCalcVar = (millis() - iTime) % 1000;
    aTime = millis() - iTime;
    if (timeCalcVar >= 245 && timeCalcVar <= 255 &&
        minutos - aTime / 60000 < 2 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (timeCalcVar >= 495 && timeCalcVar <= 510 &&
        minutos - aTime / 60000 < 4 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (timeCalcVar >= 745 && timeCalcVar <= 760 &&
        minutos - aTime / 60000 < 2 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) < 10)
      largoTono = 300;

    lcd.setCursor(1, 0);
    if (defused) {
      lcd.print(COUNTDOWN_IN);
    } else {
      lcd.print(DETONATION_IN);
    }
    // Passed Time

    lcd.setCursor(3, 1);

    ////////HERE ARE THE THREE OPTIONS THAT ENDS THE GAME///////////

    ////TIME PASED AWAY AND THE BOMB EXPLODES
    if (minutos - aTime / 60000 == 0 &&
        59 - ((aTime / 1000) % 60) == 0) // Check if game ends
    {
      if (!defused) {
        failSplash();
      }
      {
        sabotageOk();
      }
    }
    // print time
    printTime(minutos, aTime);

    //// SECOND OPTION: YOU PRESS DISARMING BUTTON

    // IF IS A PASSWORD GAME

    if ('C' == keypad.getKey() && passwordEnable) {

      cls();
      digitalWrite(REDLED, LOW);
      digitalWrite(GREENLED, HIGH);
      lcd.print(DISARMA);
      delay(1000); // a little delay to think in the password

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(ENTER_CODE);

      setCodeTime(); // we need to set the compare variable first

      // then compare :D

      if (comparePassword()) {
        if (!defused) {
          defused = true;
          sabotage();
        }
        sabotageFail();
      }
      if (errorBeforeCode) {
        endGame = true;
      } else {
        minutos = minutos / 2;
        errorBeforeCode = true;
      }

      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(CODE_ERROR);
      if (soundEnable)
        tone(tonepin, errorTone, 200);
      delay(500);
      cls();
    }

    if (wireEnable && defused) {
      int wiresStatus = wireStatus();
      Serial.println("Stato fili ");
      Serial.println(wiresStatus);
      if (wiresStatus == -1) {
        if (!defused) {
          defused = true;
          sabotage();
        }
        sabotageFail();
      } else if (wiresStatus > 0) {
        lcd.clear();
        lcd.setCursor(3, 0);
        lcd.print(WIRE_CODE_ERROR);
        if (soundEnable)
          tone(tonepin, errorTone, 200);
        delay(500);
        cls();
        if (wireErrors > 1 || wiresStatus > 1) {
          endGame = true;
        } else if (wireErrors == 1) {
          minutos = minutos / 2;
        }
      }
    }

    if (defusing && !passwordEnable) // disarming bomb
    {
      lcd.clear();
      digitalWrite(REDLED, LOW);
      lcd.setCursor(3, 0);
      lcd.print(DISARM);
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis();
      while (defusing) {
        keypad.getKey();
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000) {
          if (!defused) {
            failSplash();
          } else {
            sabotageOk();
          }
        }

        timeCalcVar = (millis() - xTime) % 1000;
        if (timeCalcVar >= 0 && timeCalcVar <= 20) {
          digitalWrite(GREENLED, HIGH);
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 500) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(GREENLED, LOW);
        }
        unsigned long seconds = (millis() - xTime);
        percent = seconds / (ACTIVATESECONDS * 10);
        drawBar(percent);

        // BOMB DISARMED RETURN TO SABOTAGE
        if (percent >= 100) {
          cls();
          lcd.print("BOMBA DISARMATA");
          delay(1000);
          sabotage();
        }
      }
      digitalWrite(REDLED, LOW);
      digitalWrite(GREENLED, LOW);
      cls();
    }
  }
}

void
stripeBlink()
{
  unsigned long now;
  now = millis();
  if (now >= nextStripeBlink) {
    nextStripeBlink = now + 500; // Next change in one second
    if (stripeState == LOW) {
      digitalWrite(STRIPELED, HIGH);
      stripeState = HIGH;
    } else {
      digitalWrite(STRIPELED, LOW);
      stripeState = LOW;
    }
  }
}

// Set the password variable
void
setCode()
{

  lcd.setCursor(0, 1);
  for (int i = 0; i < 8; i++) {
    while (1) {
      var = getNumber();
      if (var != 'X') {
        codeInput[i] = var;

        if (i != 0) {
          lcd.setCursor(i - 1, 1);
          lcd.print("*");
          lcd.print(var);
        } else {
          lcd.print(var);
        }
        tone(tonepin, 2400, 30);
        break;
      }
    }
  }
}
void
setCodeTime()
{

  timeCalcVar = millis();

  for (int i = 0; i < 8; i++) {
    while (1) {
      if (ACTIVATESECONDS * 1000 + timeCalcVar - millis() <= 100) {
        codeInput[i] = 'X';
        break;
      }

      lcd.setCursor(11, 0);
      printTimeDom(ACTIVATESECONDS * 1000 + timeCalcVar - millis(), false);

      var = getNumber();
      if (var != 'X') {
        codeInput[i] = var;

        if (i != 0) {
          lcd.setCursor(i - 1, 1);
          lcd.print("*");
          lcd.print(var);
        } else {
          lcd.print(var);
        }
        tone(tonepin, 2400, 30);
        break;
      }
    }
  }
}
void
setPass()
{

  lcd.setCursor(0, 1);

  for (int i = 0; i < 8; i++) {
    while (1) {
      var = getNumber();
      if (var != 'X') {
        password[i] = var;
        if (i != 0) {
          lcd.setCursor(i - 1, 1);
          lcd.print("*");
          lcd.print(var);
        } else {
          lcd.print(var);
        }
        tone(tonepin, 2400, 30);
        break;
      }
    }
  }
}

void
setNewPass()
{

  while (1) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("NUOVA PW");
    setPass();

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("RIPETI PW");

    setCode();

    if (comparePassword()) {

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("PW OK!");
      delay(2000);
      break;
    } else {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ERRORE PW!");
      if (soundEnable)
        tone(tonepin, errorTone, 200);
      delay(2000);
    }
  }
}

// Whait until a button is pressed if is a number return the number 'Char' if
// not return x
char
getNumber()
{
  while (1) {
    var = keypad.getKey();
    if (var) { //
      switch (var) {
        case 'A':
          return 'X';
          break;
        case 'B':
          return 'X';
          break;

        case 'C':
          return 'X';
          break;
        case 'D':
          return 'X';
          break;
        case '*':
          return 'X';
          break;
        case '#':
          return 'X';
          break;
        default:
          return var;
          break;
      }
    }
    return 'X';
  }
}

byte
getRealNumber()
{

  while (1) {
    var = keypad.waitForKey();

    if (var) { //
      switch (var) {
        case '1':
          return 1;
          break;
        case '2':
          return 2;
          break;

        case '3':
          return 3;
          break;
        case '4':
          return 4;
          break;
        case '5':
          return 5;
          break;
        case '6':
          return 6;
        case '7':
          return 7;
          break;
        case '8':
          return 8;
          break;
        case '9':
          return 9;
          break;
        case '0':
          return 0;
          break;

        default:
          return 11;
          break;
      }
    }
    return 11;
  }
}

void
enableWire()
{
  while (1) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(WIRE_CHOOSE);
    lcd.setCursor(0, 1);
    lcd.print(WIRE_SELECTION);
    while (1) {
      var = keypad.waitForKey();
      if (var == 'A') {
        tone(tonepin, 2400, 30);
        correctWire = 4;
        break;
      }
      if (var == 'B') {
        tone(tonepin, 2400, 30);
        correctWire = 5;
        break;
      }
      if (var == 'C') {
        tone(tonepin, 2400, 30);
        correctWire = 6;
        break;
      }
      if (var == 'D') {
        tone(tonepin, 2400, 30);
        correctWire = 7;
        break;
      }
    }
    printConfirmWire();
    while (1) {
      var = keypad.waitForKey();
      if (var == 'D') // Accept
      {
        tone(tonepin, 2400, 30);
        break;
      }
      if (var == 'C') // Cancel or Back Button :')
      {
        tone(tonepin, 2400, 30);
        correctWire = -1;
        cls();
        break;
      }
    }
    if (correctWire != -1) {
      break;
    }
  }
}

void
printConfirmWire()
{
  cls();
  lcd.print("Cavo ");
  lcd.print(getChoosenWireColour());
  lcd.setCursor(0, 1);
  lcd.print("scelto, ok?");
}

String
getChoosenWireColour()
{
  switch (correctWire) {
    case 4:
      return (String) "Giallo";
    case 5:
      return (String) "Verde";
    case 6:
      return (String) "Blu";
    case 7:
      return (String) "Viola";
    default:
      return (String) "NaC";
  }
}

// void getConfig(){
//
////Check first time
//
// if (EEPROM.read(0)!= 1){
////write default config values
//
//
//
//}
//
////RELAY_TIME = EEPROM.read(1) * 1000 ;
//
//
//}

// void getConfig(){
//
////Check first time
//
// if (EEPROM.read(0)!= 1){
////write default config values
//
//
//
//}
//
////RELAY_TIME = EEPROM.read(1) * 1000 ;
//
//
//}

void
search()
{

  refresh = true;
  cls();
  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  // SETUP INITIAL TIME
  int minutos = GAMEMINUTES - 1;
  unsigned long iTime = millis(); //  initialTime in millisec
  unsigned long aTime;
  // var='o';

  // Starting Game Code
  while (1) { // this is the important code, is a little messy but works good.

    // If you fail disarm.
    if (endGame) {
      failSplash();
    }

    // Code for led blinking
    timeCalcVar = (millis() - iTime) % 1000;
    if (timeCalcVar >= 0 && timeCalcVar <= 50)
      digitalWrite(GREENLED, HIGH);
    if (timeCalcVar >= 90 && timeCalcVar <= 130)
      digitalWrite(GREENLED, LOW);

    lcd.setCursor(1, 0);
    lcd.print(GAME_TIME_TOP);
    aTime = millis() - iTime;
    lcd.setCursor(2, 1);

    // PRINT TIME ON LCD

    printTime(minutos, aTime);

    //###########################CHECKINGS##################

    // Check If Game End
    if (minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0)
      failSplash();
    // Serial.println(keypad.getKey());
    // USED IN PASSWORD GAME
    if ('D' == keypad.getKey() && passwordEnable) {
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(ARMING_BOMB);
      delay(1000); // a little delay to think in the password
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(ENTER_CODE);

      setCodeTime(); // we need to set the comparation variable first it writes
                     // on codeInput[]

      // then compare :D

      if (comparePassword())
        destroy();

      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print(CODE_ERROR);
      if (soundEnable)
        tone(tonepin, errorTone, 200);
      delay(500);
      cls();
    }
    // Check If Is Activating
    while (defusing && !passwordEnable) {
      digitalWrite(GREENLED, LOW);
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(ARMING_BOMB);
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis(); // start disabling time
      while (defusing) {
        keypad.getKey();
        percent = (millis() - xTime) / (ACTIVATESECONDS * 10);

        drawBar(percent);
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        Serial.println(millis() - xTime);
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000) {
          endGame = true;
        }
        timeCalcVar = (millis() - xTime) % 1000;

        if (timeCalcVar >= 0 && timeCalcVar <= 40) {
          digitalWrite(REDLED, HIGH);
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 520) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(REDLED, LOW);
        }

        if (percent >= 100) {
          digitalWrite(GREENLED, LOW);
          destroy(); // jump to the next gamemode
        }
      }
      cls();
      digitalWrite(REDLED, LOW);
    }
  }
}

void
destroy()
{

  lcd.clear();
  lcd.setCursor(3, 0);
  lcd.print(BOMB_ARMED);
  delay(1000);
  int minutos = BOMBMINUTES - 1;
  unsigned long iTime = millis();
  unsigned long aTime;
  int largoTono = 50;

  // MAIN LOOP
  while (1) {
    stripeBlink();

    // If you fail disarm.
    if (endGame) {
      explodeSplash();
    }

    // Led Blink
    timeCalcVar = (millis() - iTime) % 1000;
    if (timeCalcVar >= 0 && timeCalcVar <= 40) {
      digitalWrite(REDLED, HIGH);
      if (soundEnable)
        tone(tonepin, activeTone, largoTono);
    }
    if (timeCalcVar >= 180 && timeCalcVar <= 220) {
      digitalWrite(REDLED, LOW);
    }
    // Sound
    aTime = millis() - iTime;
    timeCalcVar = (millis() - iTime) % 1000;
    if (timeCalcVar >= 245 && timeCalcVar <= 255 &&
        minutos - aTime / 60000 < 2 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (timeCalcVar >= 495 && timeCalcVar <= 510 &&
        minutos - aTime / 60000 < 4 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (timeCalcVar >= 745 && timeCalcVar <= 760 &&
        minutos - aTime / 60000 < 2 && soundEnable)
      tone(tonepin, activeTone, largoTono);
    if (minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) < 10)
      largoTono = 300;

    lcd.setCursor(1, 0);
    lcd.print(DETONATION_IN);
    // Passed Time

    lcd.setCursor(3, 1);

    ////////HERE ARE THREE TWO OPTIONS THAT ENDS THE GAME///////////

    ////TIME PASED AWAY AND THE BOMB EXPLODES
    if (minutos - aTime / 60000 == 0 &&
        59 - ((aTime / 1000) % 60) == 0) // Check if game ends
    {
      explodeSplash();
    }
    // print time

    printTime(minutos, aTime);

    //// SECOND OPTION: YOU PRESS DISARMING BUTTON

    // IF IS A PASSWORD GAME

    if ('C' == keypad.getKey() && passwordEnable) {

      lcd.clear();
      lcd.setCursor(1, 0);
      lcd.print(DISARMA);
      delay(1000); // a little delay to think in the password

      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(ENTER_CODE);

      setCodeTime(); // we need to set the compare variable first

      // then compare :D

      if (comparePassword()) {
        disarmedSplash();
      }
      if (errorBeforeCode) {
        endGame = true;
      } else {
        minutos = minutos / 2;
        errorBeforeCode = true;
      }

      lcd.clear();
      lcd.setCursor(3, 0);
      lcd.print(CODE_ERROR);
      if (soundEnable)
        tone(tonepin, errorTone, 200);
      delay(500);
      cls();
    }

    if (wireEnable) {
      int wiresStatus = wireStatus();
      Serial.println("Stato fili ");
      Serial.println(wiresStatus);
      if (wiresStatus == -1) {
        disarmedSplash();
      } else if (wiresStatus > 0) {
        lcd.clear();
        lcd.setCursor(3, 0);
        lcd.print(WIRE_CODE_ERROR);
        if (soundEnable)
          tone(tonepin, errorTone, 200);
        delay(500);
        cls();
        if (wireErrors > 1 || wiresStatus > 1) {
          endGame = true;
        } else if (wireErrors == 1) {
          minutos = minutos / 2;
        }
      }
    }

    if (defusing && !passwordEnable) // disarming bomb
    {
      lcd.clear();
      digitalWrite(REDLED, LOW);
      lcd.setCursor(3, 0);
      lcd.print(DISARM);
      lcd.setCursor(0, 1);
      unsigned int percent = 0;
      unsigned long xTime = millis();
      while (defusing) {
        keypad.getKey();
        // check if game time runs out during the disabling
        aTime = millis() - iTime;
        if ((minutos - aTime / 60000 == 0 && 59 - ((aTime / 1000) % 60) == 0) ||
            minutos - aTime / 60000 > 4000000000) {
          endGame = true;
        }
        timeCalcVar = (millis() - xTime) % 1000;
        if (timeCalcVar >= 0 && timeCalcVar <= 20) {
          digitalWrite(GREENLED, HIGH);
          if (soundEnable)
            tone(tonepin, alarmTone1, 200);
        }
        if (timeCalcVar >= 480 && timeCalcVar <= 500) {
          if (soundEnable)
            tone(tonepin, alarmTone2, 200);
          digitalWrite(GREENLED, LOW);
        }
        unsigned long seconds = (millis() - xTime);
        percent = seconds / (ACTIVATESECONDS * 10);
        drawBar(percent);

        // BOMB DISARMED GAME OVER
        if (percent >= 100)
          disarmedSplash();
      }
      digitalWrite(REDLED, LOW);
      digitalWrite(GREENLED, LOW);
      digitalWrite(STRIPELED, LOW);
      cls();
    }
  }
}

int
wireStatus()
{
  int yellow = digitalRead(YELLOW_WIRE);
  int green = digitalRead(GREEN_WIRE);
  int blue = digitalRead(BLUE_WIRE);
  int violet = digitalRead(VIOLET_WIRE);
  Serial.println("Filo giusto ");
  Serial.println(correctWire);
  Serial.println("Filo giallo ");
  Serial.println(yellow);
  Serial.println("Filo verde ");
  Serial.println(green);
  Serial.println("Filo blu ");
  Serial.println(blue);
  Serial.println("Filo viola ");
  Serial.println(violet);
  int openWires = 0;
  if (yellow && !yellowWireOpened) {
    openWires++;
    yellowWireOpened = true;
  }
  if (green && !greenWireOpened) {
    openWires++;
    greenWireOpened = true;
  }
  if (blue && !blueWireOpened) {
    openWires++;
    blueWireOpened = true;
  }
  if (violet && !violetWireOpened) {
    openWires++;
    violetWireOpened = true;
  }
  if (openWires == 0) {
    return openWires;
  } else if (openWires > 2) {
    wireErrors++;
    return openWires;
  }
  Serial.println("openWires");
  Serial.println(openWires);
  switch (correctWire) {
    case 4:
      if (yellow) {
        return -1;
      }
      break;
    case 5:
      if (green) {
        return -1;
      }
      break;
    case 6:
      if (blue) {
        return -1;
      }
      break;
    case 7:
      if (violet) {
        return -1;
      }
      break;
    default:
      return openWires;
      break;
  }
  wireErrors++;
  return openWires;
}

void
explodeSplash()
{

  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  cls();
  delay(100);
  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  lcd.setCursor(5, 0);
  lcd.print(BOOM);
  lcd.setCursor(4, 1);
  lcd.print(GAME_OVER);
  if (grenadeEnable) {
    analogWrite(GRENADE, 255); // grenade explosion
  }
  for (int i = 200; i > 0;
       i--) // this is the ultra hi definition explosion sound xD
  {
    if (grenadeEnable && i == 75) { // Aspetta 75 volte per 20ms = 1,5s
      analogWrite(GRENADE, 0);
    }
    tone(tonepin, i);
    delay(20);
  }
  noTone(tonepin);
  if (relayEnable) {
    activateRelay();
  }

  endGameOrPlayAgain();
}

void
failSplash()
{

  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  cls();
  delay(100);
  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  lcd.setCursor(1, 0);
  lcd.print(TIME_OVER);
  lcd.setCursor(4, 1);
  lcd.print(GAME_OVER);
  for (int i = 200; i > 0;
       i--) // this is the ultra hi definition explosion sound xD
  {
    tone(tonepin, i);
    delay(20);
  }
  noTone(tonepin);
  if (relayEnable) {
    activateRelay();
  }

  endGameOrPlayAgain();
}

void
sabotageOk()
{

  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  cls();
  delay(100);
  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  lcd.setCursor(4, 0);
  lcd.print(BOOM);
  lcd.setCursor(1, 1);
  lcd.print(SABOTAGE_OK);
  if (grenadeEnable) {
    analogWrite(GRENADE, 255); // grenade explosion
  }
  for (int i = 200; i > 0;
       i--) // this is the ultra hi definition explosion sound xD
  {
    if (grenadeEnable && i == 75) { // Aspetta 75 volte per 20ms = 1,5s
      analogWrite(GRENADE, 0);
    }
    tone(tonepin, i);
    delay(20);
  }
  noTone(tonepin);
  if (relayEnable) {
    activateRelay();
  }

  endGameOrPlayAgain();
}

void
sabotageFail()
{

  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  cls();
  delay(100);
  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  lcd.setCursor(1, 0);
  lcd.print(SABOTAGE_FAIL);
  lcd.setCursor(4, 1);
  lcd.print(GAME_OVER);
  for (int i = 200; i > 0;
       i--) // this is the ultra hi definition explosion sound xD
  {
    tone(tonepin, i);
    delay(20);
  }
  noTone(tonepin);
  if (relayEnable) {
    activateRelay();
  }

  endGameOrPlayAgain();
}

void
endGameOrPlayAgain()
{

  while (1) {
    key = keypad.waitForKey();
    if (key == 'C') {
      break;
    }
  }
  cls();

  lcd.print(PLAY_AGAIN);
  lcd.setCursor(0, 1);
  lcd.print(YES_OR_NOT);
  while (1) {
    var = keypad.waitForKey();
    if (var == 'A') {
      tone(tonepin, 2400, 30);
      if (sdStatus) {
        startGameCount();
        search();
      }
      if (saStatus) {
        startGameCount();
        start = true; // to set iTime to actual millis() :D
        sabotage();
      }
      if (doStatus) {
        domination();
      }
      menuPrincipal();
    }
    if (var == 'B') {
      tone(tonepin, 2400, 30);
      menuPrincipal();
      break;
    }
  }
}

void
disarmedSplash()
{
  endGame = false;
  wireErrors = 0;
  yellowWireOpened = false;
  blueWireOpened = false;
  greenWireOpened = false;
  violetWireOpened = false;
  errorBeforeCode = false;
  digitalWrite(REDLED, LOW);
  digitalWrite(GREENLED, LOW);
  digitalWrite(STRIPELED, LOW);
  if (sdStatus || saStatus) {
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print(DISARM);
    lcd.setCursor(3, 1);
    lcd.print(DEFENDERS_WIN);
    digitalWrite(GREENLED, HIGH);
    delay(60000);
    digitalWrite(GREENLED, LOW);
  }
  endGameOrPlayAgain();
}

void
drawBar(byte porcent)
{

  // TODO: Optimize this code
  int box = (8 * porcent) / 10;
  lcd.setCursor(0, 1);
  while (box >= 5) {
    if (box >= 5) {
      lcd.write(4);
      box -= 5;
    }
  }
  switch (box) {
    case 0:
      break;
    case 1:
      lcd.write((uint8_t)0);
      break;
    case 2:
      lcd.write(1);
      break;
    case 3:
      lcd.write(2);
      break;
    case 4:
      lcd.write(3);
      break;
  }
}

void
cls()
{

  lcd.clear();
  lcd.setCursor(0, 0);
}

void
printTime(unsigned long minutos, unsigned long aTiempo)
{

  timeCalcVar = minutos - aTiempo / 60000;
  // Hours

  if (timeCalcVar / 60 == 0 && refresh) {
    lcd.clear();
    refresh = false;
    // delay(100);
    lcd.setCursor(3, 1);
    Serial.println("!!!!");
  }

  if (timeCalcVar / 60 >= 1) {

    if (timeCalcVar / 60 < 10) {
      lcd.setCursor(2, 1);
      lcd.print("0");
      lcd.print(timeCalcVar / 60);
    } else {
      lcd.print(timeCalcVar / 60);
    }

    lcd.print(":");
  }
  // minutes
  if (timeCalcVar % 60 < 10) {
    lcd.print("0");
    lcd.print(timeCalcVar % 60);
  } else {
    lcd.print(timeCalcVar % 60);
  }
  lcd.print(":");
  // seconds
  timeCalcVar = aTiempo / 1000;
  if (59 - (timeCalcVar % 60) < 10) {
    lcd.print("0");
    lcd.print(59 - (timeCalcVar % 60));
  } else {
    lcd.print(59 - (timeCalcVar % 60));
  }
  lcd.print(":");
  // this not mach with real time, is just a effect, it says 999 because
  // millis%1000 sometimes give 0 LOL
  lcd.print(999 - (millis() % 1000));
}

void
printTimeDom(unsigned long aTiempo, boolean showMillis)
{
  // minutes
  if ((aTiempo / 60000) < 10) {
    lcd.print("0");
    lcd.print(aTiempo / 60000);
  } else {
    lcd.print(aTiempo / 60000);
  }
  lcd.print(":");
  // seconds
  if (((aTiempo / 1000) % 60) < 10) {
    lcd.print("0");
    lcd.print((aTiempo / 1000) % 60);
  } else {
    lcd.print((aTiempo / 1000) % 60);
  }
  if (showMillis) {
    lcd.print(":");
    // this not mach with real time, is just a effect, it says 999 because
    // millis%1000 sometimes give 0 LOL
    lcd.print(999 - millis() % 1000);
  }
}

void
startGameCount()
{
  //
  lcd.clear();
  lcd.setCursor(1, 0);
  lcd.print("    SEI");
  lcd.setCursor(0, 1);
  lcd.print("    PRONTO?");
  keypad.waitForKey(); // if you press a button game start

  cls();
  lcd.setCursor(1, 0);
  lcd.print("INIZIO GIOCO");
  for (int i = 5; i > 0; i--) { // START COUNT GAME INIT
    lcd.setCursor(5, 1);
    tone(tonepin, 2000, 100);
    lcd.print("IN ");
    lcd.print(i);
    delay(1000);
  }
  cls();
}

void
checkArrows(byte i, byte maxx)
{
  //

  if (i == 0) {
    lcd.setCursor(15, 1);
    lcd.write(6);
  }
  if (i == maxx) {
    lcd.setCursor(15, 0);
    lcd.write(5);
  }
  if (i > 0 && i < maxx) {
    lcd.setCursor(15, 1);
    lcd.write(6);
    lcd.setCursor(15, 0);
    lcd.write(5);
  }
}

void
activateRelay()
{
  delay(RELAY_TIME);
}
